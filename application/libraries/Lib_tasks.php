<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lib_tasks
{
	var $CI;
	public function __construct()
	{
		$this->CI =& get_instance();
	}

	public function load_tasks($userid)
	{
		
		$result = $this->CI->mod_tasks->get_tasks('', $userid);
		if($result) {
			return $result;
		}
		else {
			return false;
		}
	}

	public function search_tasks($search_cond)
	{
		$cond['userid'] = $search_cond['userid'];

		if($search_cond['search_type'] == 'today') {
			$cond['start_date'] = date("Y-m-d 00:00:00", time());
			$cond['end_date'] = date("Y-m-d 23:59:59", time());
		}
		elseif($search_cond['search_type'] == 'yesterday') {
			$cond['start_date'] = date("Y-m-d 00:00:00", strtotime("-1 day"));
			$cond['end_date'] = date("Y-m-d 23:59:59", strtotime("-1 day"));
		}
		elseif($search_cond['search_type'] == 'thisweek') {
			list($start_date, $end_date) = $this->get_week_range('thisweek');

			$cond['start_date'] = date("Y-m-d 00:00:00", $start_date);
			$cond['end_date'] = date("Y-m-d 23:59:59", $end_date);
		}
		elseif($search_cond['search_type'] == 'lastweek') {
			list($start_date, $end_date) = $this->get_week_range('lastweek');

			$cond['start_date'] = date("Y-m-d 00:00:00", $start_date);
			$cond['end_date'] = date("Y-m-d 23:59:59", $end_date);
		}
		else {
			$cond['start_date'] = date("Y-m-d 00:00:00", strtotime($search_cond['start']));
			$cond['end_date'] = date("Y-m-d 23:59:59", strtotime($search_cond['end']));
		}
		
		$result = $this->CI->mod_tasks->search_tasks($cond);
		if($result) {
			return $result;
		}
		else {
			return false;
		}
	}

	function get_week_range($week) {
		if($week == "thisweek") {
	    	$ts = time();
		}
		else {
	    	$ts = strtotime(date("Y-m-d", time()) . " -7 days");
		}
			
	    $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
    	return array($start, strtotime('next saturday', $start));
	}



	public function save_tasks($values){
		$data['userid']			= $values['userId'];
		$data['task']			= $values['taskTitle'];
		$data['added_by']		= $values['added_by'];
		$data['insert_date']	= date("Y-m-d h:i:s", time());
		$data['status']			= PUBLISHED;

		$result = $this->CI->mod_tasks->insert_task($data);
		if($result > 0) {

			return $this->get_task_json($result);			
		}
		else {
			return $result['result'] = 'failed';

		}

	}

	public function get_task_json($id=null) {
		$result = array();
		$res = $this->CI->mod_tasks->get_tasks($id);

		if($res) {
			$result['id'] = $res[0]->id;
			$result['userid'] = $res[0]->userid;
			$result['task'] = $res[0]->task;
			$result['taskid'] = $res[0]->taskid;
			$result['insert_date'] = date("d-M-Y", strtotime($res[0]->insert_date));
			$result['username'] = $res[0]->username;
			$result['result'] = 'success';
		}
		else {
			$result['result'] = 'failed';
			
		}
		return $result;

	}

	public function update_status($values) {

	}

	public function getUsernameBytId($id='') {
		if(!empty($id)) {
			$user = $this->CI->ion_auth->user($id)->row(); 
		}
		else {
			$user = $this->CI->ion_auth->user()->row(); 			
		}

		return $user->username;
	}	

}

