<html>
<head>
  <title>login - Task manager</title>
    <style type="text/css">
    #login_form{
      width: 400px;
      min-height: 260px;
      margin: 50px auto 0 auto;
      border: 1px solid #CCC;
      font-family: arial;
      color: #666;
    }

    #login_form .title {
      margin: 0;
      padding: 0 10px;
      font-size: 22px;
      height: 35px;
      line-height: 35px;
      background: #CCC;
    }

    #login_form .inner{
      padding: 10px;

    }

    #login_form .inner .text{
      color: #666;
      font-size: 14px;
    }


    #login_form .inputs label{
      display: block;
      font-size: 13px;
    }
    #login_form .inputs input{
      display: block;
      font-size: 14px;
      width: 300px;
      height: 30px;
      border: 1px solid #CCC;
      background: #FAFAFA;
      padding: 0 5px;
    }

    #login_form .forgot a{
      color: #035EFF;
      text-decoration: none;
      font-size: 12px;

    }
    #login_form .remember{
      float: right;
      margin-right: 80px;
    }

    #login_form .btn{
      margin-top: 15px;
    }
    </style>
</head>
<body>
    <div id="login_form">
      <h1 class="title">Login</h1>
      <div class='inner'>
        <p class="text">Please login with your email/username and password below.</p>

        <div id="infoMessage"><?php echo $message;?></div>

        <?php echo form_open("auth/login");?>
          <div class="inputs">
            <label class="label" for="identity">Username:</label>
            <?php echo form_input($identity);?>
            

            <label class="label" for="password">Password:</label>
            <?php echo form_input($password);?>
          </div>
          
          <p class="remember">
          <label class="label"><?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?> Remember me</label>

          </p>


          <div class="btn">
            <?php echo form_submit('submit', 'Login');?>
            <div class="forgot"><a class="password" href="forgot_password">Forgot your password?</a></div>
          </div>

        <?php echo form_close();?>
        
      </div>
    </div>
    <div style="display:none;">Page rendered in {elapsed_time} seconds</div>
</body>    
</html>