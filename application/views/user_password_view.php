<div id="infoMessage"><?php echo $message;?></div>
<div class="user-managment update_info">
	<?php echo form_open("user/update_user");?>
		<h3> Change Information</h3>
		<label>First Name</label>
		<input class="text-field" name="first_name" id="first_name" type="text" value="<?php echo $user_info['first_name']; ?>" placeholder="Firstname" />
		
		<label>Last name</label>
		<input class="text-field" name="last_name" id="last_name" type="text" value="<?php echo $user_info['last_name']; ?>" placeholder="Lastname" />
		
		<label>Username</label> 
		<input class="text-field" name="username" id="username" type="text" value="<?php echo $user_info['username']; ?>" placeholder="Username" />
		
		<label>Email</label>
		<input class="text-field" name="email" id="email" type="text" value="<?php echo $user_info['email']; ?>" placeholder="Email" />
		<br>
		<input class="save" type="submit" name="user_info" id="a" value="Update Info" />
		<input type="hidden" name="userid" value="<?php echo $userid;?>" />
	<?php echo form_close();?>
</div>
<div class="user-managment update_pass" style="margin-bottom:10px;">
	<?php echo form_open("user/update_password");?>
		<h3> Change Password</h3>
		<input class="text-field" name="password" id="password" type="password" placeholder="Password" />
		<br>
		<input class="text-field" name="password_confirm" id="password_confirm" type="password" placeholder="Password confirm" />
		<br>
		<input class="save" type="submit" name="user_pass" id="b" value="Update Password" />
		<input type="hidden" name="userid" value="<?php echo $userid;?>" />
	<?php echo form_close();?>
</div>
<?php if($this->ion_auth->is_admin()) : ?>
<div class="user-managment update_pass">
	<?php echo form_open("user/update_permission");?>
		<h3>Change Permission</h3>
		<?php 
		//echo "<pre>";print_r($group->name);
		//exit();
		?>
		<label><input name="permission" type="radio" value="admin" <?php echo ($group['name'] == "admin") ? 'checked' : '';?> />Admin</label>
		<label><input name="permission" type="radio" value="user" <?php echo ($group['name'] == "members") ? 'checked' : '';?> />User</label>
		<br>
		<input class="save" type="submit" name="update_permission" value="Update Permission" />
		<input type="hidden" name="userid" value="<?php echo $userid;?>" />
	<?php echo form_close();?>
</div>
<?php endif; ?>