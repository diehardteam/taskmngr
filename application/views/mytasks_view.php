<script type="text/javascript">
/* when the DOM is ready */
jQuery(document).ready(function() {


	/* grab important elements */
	var sortInput 	= jQuery('#sort_order');
	var submit 		= true; //autosubmit=true/false
	var messageBox 	= jQuery('#message-box');
	var list 		= jQuery('#task_listing');



	/* create requesting function to avoid duplicate code */
	var request = function() {
		jQuery.ajax({
			beforeSend: function() {
				jQuery(".saving").css("display", "block");
			},
			complete: function() {
				jQuery(".saving").css("display", "none");
			},
			data: 'sort_order=' + sortInput[0].value + '&do_submit=1&byajax=1&userid=<?php echo $userid;?>',
			type: 'post',
			url: site_url + '/tasks/sort_order'
		});
	};

	/* worker function */
	var fnSubmit = function(save) {
		var sortOrder = [];
		list.children('li').each(function(){
			sortOrder.push(jQuery(this).data('id'));
		});
		sortInput.val(sortOrder.join(','));
		console.log(sortInput.val());
		if(save) {
			request();
		}
	};

	/* store values */
	list.children('li').each(function() {
		var li = jQuery(this);
		li.data('id',li.attr('title')).attr('title','');
	});

	/* sortables */
	list.sortable({
		opacity: 0.7,
		update: function() {
			fnSubmit(true);
		}
	});

	list.disableSelection();

	/* ajax form submission */
	jQuery('#dd-form').bind('submit',function(e) {
		if(e) e.preventDefault();
		fnSubmit(true);
	});
});
</script>

<script id="task_item_tpl" type="text/xml">
	<li id="task_{{taskid}}" class="present" style="background:#FDFDD0;">
		<p class="task_txt">{{task}}</p>
		<div class="action">
			<a class="btn" href="javascript:void(0);" onclick="trash({{taskid}},{{userid}});">remove</a>
			<a class="btn status" href="javascript:void(0);" onclick="done({{taskid}},{{userid}});">done</a>
			<a class="btn_info" href="javascript:void(0);">
				<img src="<?php echo base_url();?>assets/img/icon-circle-bg.png">
				<div class="info">
					<div class="arrow"></div>
					<span>added by {{username}}</span> </br>
					<span>on {{insert_date}}</span>
				</div>
			</a>
		</div>
	</li>
</script>

<div class="details">
	<div class="form">
		<form id="taskForm" action="" method="POST">
			<textarea class="text-field" name="todo" id="todo">Click to add todo...</textarea>
			<input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>" />
			<input type="hidden" name="sign" id="sign" value="<?php echo time();?>" />
		</form>
	</div>
	<?php 
	//echo '<pre>'; print_r($task_items); echo "</pre>";
	?>
	<div class="example">
		<form id="dd-form" method="post" action="">
		<ol id="task_listing" class="ui-sortable" unselectable="on">
			<?php
			$order = array();
			if(isset($task_items) && !empty($task_items)){
				foreach ($task_items as $task_item) {
				?>
				<li title="<?php echo $task_item->taskid;?>" id="task_<?php echo $task_item->taskid;?>" class="present <?php echo ($task_item->status == DONE) ? 'done' : '';?>">
					<p class="task_txt"><?php echo $task_item->task;?></p>
					<div class="action">
						<a class="btn" href="javascript:void(0);" onclick="trash(<?php echo $task_item->taskid.', '.$userid;?>);">remove</a>
						<?php if($task_item->status == DONE) { ?>
							<a class="btn status" href="javascript:void(0);" onclick="undone(<?php echo $task_item->taskid.", ".$userid;?>);">undone</a>
						<?php }	else { ?>
						<a class="btn status" href="javascript:void(0);" onclick="done(<?php echo $task_item->taskid.", ".$userid;?>);">done</a>
						<?php } ?>

						<a class="btn_info" href="javascript:void(0);">
							<img src="<?php echo base_url();?>assets/img/icon-circle-bg.png">
							<div class="info">
								<div class="arrow"></div>
								<span>added by <?php echo $task_item->username;?></span> </br>
								<span>on <?php echo date("d-M-Y", strtotime($task_item->insert_date));?></span>
							</div>
						</a>
					</div>
				</li>
			<?php 
				$order[] = $task_item->taskid;
				}
			}
			?>
		</ol>
		<input type="hidden" name="sort_order" id="sort_order" value="<?php echo implode(',',$order); ?>" />
		</form>
	</div>
	<div class="saving"><!-- start saving -->
		<span class="load_txt">Saving...</span>
		<a class="loading" href="#"> <img src="<?php echo base_url();?>assets/img/ajaxload.gif"></a>
	</div><!-- End saving -->
</div><!-- End details -->