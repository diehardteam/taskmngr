<!DOCTYPE html>
<html lang="en">
<head>
<meta charset=utf-8>
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
<title>Edit Profile</title>
<link href="css/style.css" rel="stylesheet" />

</head>

<body>
	<div id="dotcom-heroes">
		<div class="wrap">
			<div id="header"><!-- Start header -->
				<a class="logo" href="#"><img src="img/logo-bg.png"></a>
				<div class="name-opt">
					<span>Danny</span>
					<a class="down-arrow" href="#"><img src="img/down-select-btn-bg.png"></a>
				</div>
			</div> <!-- End Header -->
			<div id="nav">
				<div class="admin-opt">
					<label>
						<a href="#"> My to do</a>
						<div class="my-to-do">
							<span>Admin option</span>
							<a class="down-arrow" href="#"><img src="img/down-select-btn-bg.png"></a>
						</div>
					</label>
				</div>
			</div>

			<div id="content" class="clr">
				<div class="details">
					<div class="form">
						<form>
							<input class="text-field" name="password" id="p" type="text" placeholder="password..." />
							<input class="text-field" name="new-password" id="p" type="text" placeholder="new password again..." />
							<input class="save-btn" type="submit" name="save" id="s" value="save" />
						</form>
					</div>
					
				</div><!-- End details -->
				<div class="left-sideber">
					<div class="name-list">
						<ul>
							<li class="curent"> <a href="#">Danny</a> </li>
							<li> <a href="#">Jobaer</a> </li>
							<li> <a href="#">Jochem</a> </li>
						</ul>
					</div>
				</div><!-- End left sideber -->
			</div>
		</div>
	</div>
	 
</body>
</html>