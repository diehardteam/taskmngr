<script type="text/javascript">
jQuery(document).ready(function() {
	$(".search_type").click(function(){


    	jQuery("#task_listing").html('<img class="task_loading" src="<?php echo base_url();?>assets/img/ajax-loading.gif" />');

		$(".search_type").removeClass("active");
		$(this).addClass("active");
		
		var dataString='';
		var search_type = $(this).attr("rel");

		if(search_type == 'range') {
			$(".search_box").slideDown();
			jQuery("#task_listing").html('');
			return false;
		}
		else {
			$(".search_box").css("display", "none");
			dataString = 'search_type=' + search_type;
		}

		search_tasks(dataString);
		return false;
	});

	$("#search_range_btn").click(function(){
    	jQuery("#task_listing").html('<img class="task_loading" src="<?php echo base_url();?>assets/img/ajax-loading.gif" />');
		var from = $("#from").val();
		var to = $("#to").val();
		var dataString = 'search_type=range&start='+from+'&end='+to;

		search_tasks(dataString);
		return false;
	});

	
    $( "#from" ).datepicker({
        defaultDate: "-1w",
        dateFormat: "yy-mm-dd",
        changeMonth: false,
        numberOfMonths: 2,
        maxDate: "-1d",
        minDate: "-1m",
        onSelect: function( selectedDate ) {
            $( "#to" ).datepicker( "option", "minDate", selectedDate );
        }
    });
    $( "#to" ).datepicker({
        defaultDate: "0",
        dateFormat: "yy-mm-dd",
        changeMonth: false,
        numberOfMonths: 2,
        maxDate:0,
        onSelect: function( selectedDate ) {
            $( "#from" ).datepicker( "option", "maxDate", selectedDate );
        }
    });

    /*********** Default ************/
    search_tasks('search_type=today');


}); //End document ready


function search_tasks(dataString) {
	<?php $userid = (isset($_GET['uid']) && $_GET['uid'] > 0 && $this->ion_auth->is_admin()) ? $_GET['uid'] : $this->user->id; ?>
	dataString = dataString + "&uid=<?php echo $userid;?>";
	$.ajax({  
		type: "POST",  
		url: site_url + "/tasks/search_history",  
		data: dataString,  
		success: function(json) {  
			$(".saving").css("display", "none");
			var output = Mustache.render(jQuery("#task_item_tpl").html(), json);
			jQuery("#task_listing").html(output);
		}  
	});
}

</script>

<script id="task_item_tpl" type="text/xml">
	{{#data}}
	<li id="task_{{taskid}}" class="present done" style="">
		<p class="task_txt">{{task}}</p>
		<div class="action">
			<!--a class="btn" href="javascript:void(0);" onclick="trash({{taskid}},{{userid}});">remove</a>
			<a class="btn status" href="javascript:void(0);" onclick="done({{taskid}},{{userid}});">done</a-->
			<a class="btn_info" href="javascript:void(0);">
				<img src="<?php echo base_url();?>assets/img/icon-circle-bg.png">
				<div class="info">
					<div class="arrow"></div>
					<span>added by {{username}}</span> </br>
					<span>on {{insert_date}}</span>
				</div>
			</a>
		</div>
	</li>
	{{/data}}
</script>

<div class="details">
	
	<form id="searchForm" action="" method="POST">
		<div class="history">
			<ul>
				<li><a class="search_type" href="#" rel="today">today</a></li>
				<li><a class="search_type" href="#" rel="yesterday">yesterday</a></li>
				<li><a class="search_type" href="#" rel="thisweek">this week</a></li>
				<li><a class="search_type" href="#" rel="lastweek">last week</a></li>
				<li><a class="search_type" href="#" rel="range">date range</a></li>
				<div style="clear:both;"></div>
			</ul>
			<div class="search_box">
				<label>From <input type="text" id="from" name="from" placeholder="From"></label>
				<label>To <input type="text" id="to" name="to" placeholder="To"></label>
				<input type="button" id="search_range_btn" name="search_range_btn" value="Go">
			</div>
		</div>
		
	</form>
	<?php 
	//echo '<pre>'; print_r($task_items); echo "</pre>";
	?>
	<div class="example">
		<hr>
		<ol id="task_listing">
			
		</ol>
	</div>
	<div class="saving"><!-- start saving -->
		<span class="load_txt">Saving...</span>
		<a class="loading" href="#"> <img src="<?php echo base_url();?>assets/img/ajaxload.gif"></a>
	</div><!-- End saving -->
</div><!-- End details -->