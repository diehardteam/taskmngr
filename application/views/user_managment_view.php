				<div class="details">
					<div id="infoMessage"><?php echo $message;?></div>
			
					<div class="user-managment">
						<?php echo form_open("user/manage");?>
							<input class="text-field" name="username" id="username" type="text" value="<?php echo set_value('username'); ?>" placeholder="Username" />
							<input class="text-field" name="email" id="email" type="text" value="<?php echo set_value('email'); ?>" placeholder="Email" />
							<input class="text-field" name="password" id="password" type="password" placeholder="Password" />
							<input class="text-field" name="password_confirm" id="password_confirm" type="password" placeholder="Password confirm" />
							<input class="save" type="submit" name="user" id="a" value="add user" />
						<?php echo form_close();?>
					</div>
					<hr>
					<div class="user_list">
						<ul>
							<?php
							$users = $this->ion_auth->users()->result();
							foreach ($users as $user_list) {

								?>
								<li class="user">
									<?php if($user_list->active) { ?>
									<span><?php echo $user_list->id;?>. <?php echo $user_list->username;?></span> 
									<?php } else { ?>
									<span style="text-decoration:line-through;"><?php echo $user_list->id;?>. <?php echo $user_list->username;?></span> 
									<?php } ?>
									<span style="color:#666; font-size:12px;">(<?php echo $user_list->email;?>)</span>

									<div class="action">
										<a class="btn" href="<?php echo site_url('user/edit/?uid='.$user_list->id);?>">edit</a>
										<?php if($user_list->active) { ?>
											<a class="btn" href="<?php echo site_url('user/deactivate/'.$user_list->id);?>">deactivate</a>
										 <?php } else { ?>
											<a class="btn" href="<?php echo site_url('user/activate/'.$user_list->id);?>">activate</a>
										<?php } ?>
									</div>
								</li>
								<?php
							}
							?>
						</ul>
					</div>
					<div class="saving"><!-- start saving -->
						<span>saving</span>
						<a class="loading" href="#"> <img src="img/loading-bg.png"></a>
					</div><!-- End saving -->
				</div><!-- End details -->