<!DOCTYPE html>
<html lang="en">
<head>
<meta charset=utf-8>
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
<title>{title}</title>
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" />

<script type="text/javascript">
var site_url = '<?php echo site_url();?>';
</script>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />

<script src="<?php echo base_url();?>assets/js/jquery-1.8.2.min.js"></script>
<script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/js/mustache.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
	$("#sh-user-menu").click(function(){
		$(".user-menu").toggle("slow");
		return false;
	});
	$("#sh-user-menu").focusout(function(){
		$(".user-menu").hide("slow");
	});

	$("#btn_admin_menu").click(function(){
		$(".admin_menu").toggle("slow");
		return false;
	});
	$("#btn_admin_menu").focusout(function(){
		$(".admin_menu").hide("slow");
	});
});

</script>
</head>

<body>
	<div id="dotcom-heroes">
		<div class="wrap">
			<div id="header"><!-- Start header -->
				<a class="logo" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/logo-bg.png"></a>
				<div class="name-opt">
					<a id="sh-user-menu" class="down-arrow" href="#">
						<span><?php echo $this->user->username;?></span>
						<img src="<?php echo base_url();?>assets/img/down-select-btn-bg.png">
					</a>
					<div class="user-menu">
						<li><a href="<?php echo site_url('user/edit');?>">Edit Profile</a></li>
						<li><a href="<?php echo site_url();?>/auth/logout">Logout</a></li>
					</div>
				</div>
			</div> <!-- End Header -->
			<div id="nav">
				<div class="admin-opt">						
					<a class="menu" href="<?php echo site_url();?>">My to do</a>
					<a class="menu" href="<?php echo site_url('tasks/history');?>">History</a>
					<?php if($this->ion_auth->is_admin()) : ?>
					<div class="my-to-do">
						<a id="btn_admin_menu" class="down-arrow" href="#">
							<span>Admin option</span>
							<img src="<?php echo base_url();?>assets/img/down-select-btn-bg.png">
						</a>
						<div class="admin_menu">
							<li><a href="<?php echo site_url('user/manage');?>">User Management</a></li>
						</div>
					</div>
				<?php endif; ?>
				</div>
			</div>

			<div id="content" class="clr">
			<?php if(isset($sidebar)) : ?>
				<div class="left-sideber">
					<div class="name-list">
						<ul>
							<?php
							$users = $this->ion_auth->users()->result();
							foreach ($users as $user_list) {

								if(isset($page) && $page == 'history') {
									$link = site_url('tasks/history/?uid='.$user_list->id);
								}
								else {
									$link = site_url('tasks/index/?uid='.$user_list->id);
								}

							?>
								<li> 
									<a class="<?php echo ($userid == $user_list->id) ? 'active' : '';?>" href="<?php echo $link;?>"><?php echo $user_list->username;?></a> 
								</li>
							<?php
							}
							?>
						</ul>
					</div>
				</div><!-- End left sideber -->
			<?php endif; ?>

				{content}				
				
			</div>
		</div>
	</div>
	 <div style="display:none;">Page rendered in {elapsed_time} seconds</div>
</body>
</html>