<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_users extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function activate($id)
	{
		$this->db->where('id', $id);
		$this->db->update('users', array('active' => 1));
		return $this->db->affected_rows();
	}

	public function deactivate($id)
	{
		$this->db->where('id', $id);
		$this->db->update('users', array('active' => 0));
		return $this->db->affected_rows();
	}

	public function update_info($id, $values) {
		$this->db->where("id", $id);
		$this->db->update('users', $values);
	}

	function setgroup($type, $uid) {
		$this->db->delete('users_groups', array('user_id' => $uid)); 

		$admin = array(
		      'user_id' => $uid,
		      'group_id' => 1
		    );
		$user = array(
		      'user_id' => $uid,
		      'group_id' => 2
		    );
		if($type == 'user') {
			$this->db->insert('users_groups', $user); 
		}
		elseif($type == 'admin'){
			$data = array($admin, $user);
			$this->db->insert_batch('users_groups', $data); 
		}
		else {
			return false;
		}
		return true;
	}

	public function update_password($userid, $pass){
		$data = array('password' => $pass);
		$this->db->where("id", $userid);
		$this->db->update('users', $data);
	}
}