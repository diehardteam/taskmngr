<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_tasks extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function get_tasks($tid='', $userid='')
	{
		if(!empty($tid)) {
			$this->db->where("tasks.id", $tid);
		}

		if(!empty($userid)) {
			$this->db->where("tasks.userid", $userid);
		}

		$this->db->select('*');
		$this->db->select('tasks.id as taskid');
		$this->db->from('tasks');
		$this->db->join('users', 'users.id = tasks.added_by');
		$this->db->where("tasks.status", PUBLISHED);
		$this->db->order_by("sort_order", "asc");
		$this->db->order_by("tasks.id", "desc"); 

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
        	return $query->result();
        }
        else {
        	return false;
        }
	}

	public function search_tasks($search){
		if(!empty($search['userid'])) {
			$this->db->where("tasks.userid", $search['userid']);

		}

		if(!empty($search['start_date'])) {
			$this->db->where('tasks.insert_date >=', $search['start_date']);
		}

		if(!empty($search['end_date'])) {
			$this->db->where('tasks.insert_date <=', $search['end_date']);
		}

		$this->db->select('*');
		$this->db->select('tasks.id as taskid');
		$this->db->from('tasks');
		$this->db->join('users', 'users.id = tasks.added_by');
		$this->db->where("tasks.status", DONE);
		$this->db->order_by("tasks.id", "desc");

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
        	return $query->result();
		}
		else {
			return false;
		}
	}

	public function insert_task($values) {

		$this->db->insert('tasks', $values);
		return $this->db->insert_id();
	}

	public function update_status($values) {
		$this->db->where("id", $values['id']);
		$this->db->where("userId", $values['userId']);

		$data = array('status' => $values['status']);

		$res = $this->db->update('tasks', $data);
		if($res) {
			return true;
		}
		else {
			return false;
		}
	}

	public function set_sort_order($userid, $sort_order) {
		$ids = explode(',', $sort_order);
		/* run the update query for each id */
		foreach($ids as $index=>$id) {
			$id = (int) $id;
			if($id != '') {
				$data = array('sort_order' => $index + 1);
				$this->db->where("userId", $userid);
				$this->db->where("id", $id);
				$this->db->update('tasks', $data);
				//$this->db->free_result();
			}
		}
	}
}




