<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		$this->user = $this->ion_auth->user()->row();
		$this->load->library('form_validation');
		$this->load->model('mod_users');

	}

	public function index(){
		$this->manage();
	}

	public function manage(){
		$userid = (isset($_GET['uid']) && $_GET['uid'] > 0 && $this->ion_auth->is_admin()) ? $_GET['uid'] : $this->user->id;
		$data['userid'] = $userid;

		$data["title"] = 'Manage User';

		

		//validate form input
		$this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required');

		if ($this->form_validation->run() == true)
		{
			$username = strtolower($this->input->post('username'));
			$email    = $this->input->post('email');
			$password = $this->input->post('password');			
		}

		if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email))
		{ 
			//check to see if we are creating the user
			//redirect them back to the admin page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("user/manage", 'refresh');
		}
		else
		{ 
			//display the create user form
			//set the flash data error message if there is one
			$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$data['username'] = array(
				'name'  => 'username',
				'id'    => 'username',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('username'),
			);
			$data['email'] = array(
				'name'  => 'email',
				'id'    => 'email',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('email'),
			);
			$data['password'] = array(
				'name'  => 'password',
				'id'    => 'password',
				'type'  => 'password',
				'value' => $this->form_validation->set_value('password'),
			);
			$data['password_confirm'] = array(
				'name'  => 'password_confirm',
				'id'    => 'password_confirm',
				'type'  => 'password',
				'value' => $this->form_validation->set_value('password_confirm'),
			);

			//$this->load->view('auth/create_user', $this->data);
			$data["content"] = $this->load->view(USERMANAGE, $data, true);
			$this->parser->parse(TEMPLATE, $data);
		}
	}

	public function edit(){
		$userid = (isset($_GET['uid']) && $_GET['uid'] > 0 && $this->ion_auth->is_admin()) ? $_GET['uid'] : $this->user->id;
		$data['userid'] = $userid;
		$data['message'] = '';
		$data['group'] = $this->ion_auth->get_users_groups($userid)->row_array();

		$data["title"] = 'User Management';
		
		$data["user_info"] = $this->ion_auth->user($userid)->row_array();

		$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$data["content"] = $this->load->view(CHANGEPASS, $data, true);
		$this->parser->parse(TEMPLATE, $data);
	}

	function update_user(){
		$userid		= (int) $this->input->post('userid');
		if($userid < 1) {
			$this->session->set_flashdata('message', "<p>User ID Invalid</p>");
			redirect('user/edit', 'refresh');
		}
	
		$values['first_name'] 	= $this->input->post('first_name');
		$values['last_name'] 	= $this->input->post('last_name');
		$values['username'] 	= $this->input->post('username');
		$values['email'] 		= $this->input->post('email');
	
		
		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
		$this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');

		if ($this->form_validation->run()){
			$this->mod_users->update_info($userid, $values);
			$this->session->set_flashdata('message', "<p class='success'>Update Sucess</p>");
		}
		else {
			$this->session->set_flashdata('message', "<p>Update Failed</p>");
		}
		redirect(site_url('user/edit/?uid='.$userid), 'refresh');
	}

	function update_password(){
		$userid		= (int) $this->input->post('userid');
		if($userid < 1) {
			$this->session->set_flashdata('message', "<p>User ID Invalid</p>");
			redirect('user/edit', 'refresh');
		}


		$this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required');
		if ($this->form_validation->run()){
			$this->session->set_flashdata('message', "<p class='success'>Password Update Sucess</p>");
			$password = $this->ion_auth_model->hash_password($this->input->post('password'));
			$this->mod_users->update_password($userid, $password);
		}
		else {
			$this->session->set_flashdata('message', "<p>Password Update Failed</p>");
		}
		redirect(site_url('user/edit/?uid='.$userid), 'refresh');
	}

	public function activate($id){
		if ($this->ion_auth->is_admin()){
			$activation = $this->mod_users->activate($id);
		}

		if ($activation) {
			//redirect them to the auth page
			$this->session->set_flashdata('message', "<p>Avtivation success</p>");
		}
		else{
			$this->session->set_flashdata('message', "<p>Avtivation failed!</p>");
		}
		redirect("user/manage", 'refresh');
	}

	public function deactivate($id){

		$user = $this->ion_auth->user()->row();

		if($user->id == $id) {
			$this->session->set_flashdata('message', '<p>You can\'t deactivate yourself! </p>');
		}
		else if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
			$deactivation = $this->mod_users->deactivate($id);
			if($deactivation) {
				$this->session->set_flashdata('message', "<p>Acount deactivated</p>");
			} else {
				$this->session->set_flashdata('message', "<p>Deactivation failed!</p>");
			}
		}

		redirect("user/manage", 'refresh');
	}

	public function update_permission() {
		if(isset($_POST['permission']) && !empty($_POST['permission']) && isset($_POST['userid'])) {
			$res = $this->mod_users->setgroup($_POST['permission'], $_POST['userid']);
			if($res){
				$this->session->set_flashdata('message', '<p class="success">User permission updated</p>');
			}
			else {
				$this->session->set_flashdata('message', '<p>User permission update failed!</p>');
			}
		}
		redirect("user/edit/?uid=".$_POST['userid'], 'refresh');
	}
}