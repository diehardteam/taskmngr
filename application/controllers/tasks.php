<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Tasks extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);

		if (!$this->ion_auth->logged_in()) {
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		$this->user = $this->ion_auth->user()->row();
	}
	
	public function index()
	{
		$data['sidebar'] = true;
		
		$userid = (isset($_GET['uid']) && $_GET['uid'] > 0 ) ? $_GET['uid'] : $this->user->id;
		$data['userid'] = $userid;	
		$data['task_items'] = $this->lib_tasks->load_tasks($userid);
		$data["title"] = 'Tasks';
		$data["content"] = $this->load->view(MYTASK, $data, true);
		$this->parser->parse(TEMPLATE, $data);
	}

	public function history()
	{
		$data['sidebar'] = true;
		$data['page'] = "history";
		
		$userid = (isset($_GET['uid']) && $_GET['uid'] > 0 && $this->ion_auth->is_admin()) ? $_GET['uid'] : $this->user->id;
		$data['userid'] = $userid;
		$data["title"] = 'Tasks';
		$data["content"] = $this->load->view(HISTORY, $data, true);
		$this->parser->parse(TEMPLATE, $data);
	}

	public function new_task()
	{
		if(isset($_POST['sign'])){
			$values['taskTitle'] = $this->input->post('task');
			$values['userId'] = $this->input->post('userid');
			$values['added_by'] = $this->user->id;

			$result = $this->lib_tasks->save_tasks($values);
			header('Content-type: application/json');
			echo json_encode($result);
		}
	}

	public function change_status()
	{
		if(isset($_POST['status'])){
			$values['id'] 		= $this->input->post('taskid');
			$values['userId'] 	= $this->input->post('userid');

			if($_POST['status'] == 'trash') {
				$values['status'] = TRASH;
			}
			else if($_POST['status'] == 'done') {
				$values['status'] = DONE;
			}
			else if($_POST['status'] == 'undone') {
				$values['status'] = UNDONE;
			}
			else {
				$values['status'] = UNDONE;
			}	

			$res = $this->mod_tasks->update_status($values);
			if($res){
				$result['status'] = 'success';
			}
			else {
				$result['status'] = 'failed';
			}

			header('Content-type: application/json');
			echo json_encode($result);
		}
	}

	public function search_history(){
		$result = array();
		$userid = (isset($_REQUEST['uid'])) ? $_REQUEST['uid'] : $this->user->id;
		$data['userid'] = $userid;	
		$data['search_type'] = $this->input->post("search_type");	
		$data['start'] 	= $this->input->post("start");
		$data['end'] 	= $this->input->post("end");

		$result['data'] = $this->lib_tasks->search_tasks($data);
		
		if($result['data']) {
			$result['result'] = "success";
		}
		else {
			$result['result'] = "failed";
		}


		header('Content-type: application/json');
		echo json_encode($result);
	}

	public function sort_order(){
		if(isset($_POST['do_submit'])) {
			$userid 	= $this->input->post('userid');
			$sort_order = $this->input->post('sort_order');

			$this->mod_tasks->set_sort_order($userid, $sort_order);
		}
	}

	public function testpad(){
		echo $this->lib_tasks->getUsernameBytId();
	}
}
