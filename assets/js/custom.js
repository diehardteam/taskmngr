jQuery(document).ready(function($) {

	$("#todo").focus(function(){
		if($(this).val() == 'Click to add todo...' || $(this).val() == '') {
			$(this).val('');
			$(this).animate({
			    width: "500px",
			    height: "50px"
			}, 500 );
		}
		else {

		}
	});

	$("#todo").focusout(function(){
		if($(this).val() == 'Click to add todo...' || $(this).val() == '') {
			$(this).val('Click to add todo...');
			$(this).animate({
			    width: "250px",
			    height: "25px"
			}, 500 );
		}
	});

	/*
	|
	| Admin menu
	|
	*/
	$(".btn_info").live("click", function(){
		//alert("x");
		$(this).children(".info").toggle();
	});

	$(".btn_info").live('focusout', function(){
		$(this).children(".info").css("display", "none");
	});

	/*
	|
	| Add new task
	|
	*/
	$('#todo').keypress(function(e) {
		if (e.keyCode == 13 && !e.shiftKey) {
	    	e.preventDefault();
	    	var userid 		= $("#userid").val();
			var task_data 	= $("#todo").val();
			var sign 		= $("#sign").val();

			if(task_data.length < 1) {
				alert("Write something!");
				return false;
			}

			var dataString 	= 'task='+ task_data + '&userid=' + userid + '&sign=' + sign;  
			//alert (dataString);return false;  
			$(".load_txt").html("Saving...");
			$(".saving").css("display", "block");
			$.ajax({  
				type: "POST",  
				url: site_url + "/tasks/new_task",  
				data: dataString,  
				success: function(json) {  
					$(".saving").css("display", "none");
					$("#todo").val('');
					get_new_task(json);
				}  
			});  
			return false;
		}
	});

    $(".present").live({
        mouseenter:
           function()
           {
				jQuery(this).find(".btn_info").animate({opacity: 1});
           },
        mouseleave:
           function()
           {
           		jQuery(this).find(".btn_info").animate({opacity: 0});
           }
       }
    );

}); //END DOCUMENT READY

function get_new_task(data) {
	var output = Mustache.render(jQuery("#task_item_tpl").html(), data);
    jQuery("#task_listing").prepend(output);
}

/*
|
| Trash task
|
*/
function trash(taskId, userid){
	$(".load_txt").html("Removing...");
	$(".saving").css("display", "block");

	var dataString 	= 'taskid='+ taskId +'&userid='+ userid + '&status=trash';  

	$.ajax({  
		type: "POST",  
		url: site_url + "/tasks/change_status",  
		data: dataString,  
		success: function(json) {  
			$(".saving").css("display", "none");
			$("#task_"+taskId).css("background", "#FCC");
			$("#task_"+taskId).slideUp("slow");
			//window.location.reload();
		}  
	});  
	return false; 
}

/*
|
| Done task
|
*/
function done(taskId, userid){
	$(".load_txt").html("Requesting...");
	$(".saving").css("display", "block");

	var dataString 	= 'taskid='+ taskId + '&userid='+ userid + '&status=done';  

	$.ajax({  
		type: "POST",  
		url: site_url + "/tasks/change_status",  
		data: dataString,  
		success: function(json) {  
			$(".saving").css("display", "none");
			$("#task_"+taskId).addClass("done");
			$("#task_"+taskId+" .status").html("undone");
			//window.location.reload();
		}  
	});  
	return false; 
}

/*
|
| Undone task
|
*/
function undone(taskId, userid){
	$(".load_txt").html("Requesting...");
	$(".saving").css("display", "block");
	console.log(userid);
	var dataString 	= 'taskid='+ taskId + '&userid='+ userid + '&status=undone'; 

	$.ajax({  
		type: "POST",  
		url: site_url + "/tasks/change_status",  
		data: dataString,  
		success: function(json) {  
			$(".saving").css("display", "none");
			$("#task_"+taskId).removeClass("done");
			$("#task_"+taskId+" .status").html("done");
			//window.location.reload();
		}  
	});  
	return false; 
}




